This is a final project of Accenture AWS bootcamp
#School Chem Lab Inventory System.

Team:

1. Database(start tables) - Ingars 
/utils/dynamo
2. Handlers code - Dominiks
3. Functions to serverless - Asta
5. Testing - Dmitry

*all code is team work, we just had devided fields of responsobility

Project sceleton forked from our mentor Janis

Tools: VScode, serverless, dynamoDB, GitLab, Postman, AWS

Language: nodeJS, with a slight touch of noSQL and JSON

What was done?
-configurated serverless.yaml(properties, functions, resourses)
-set table confugurations and sheemas
-wrote Handlers


Additionally: hendlers/v1 handler folder is hardcoded and functions
are commented out in serverless.yaml, but those can come
quite handy if you need to test if your local server is running,
but make sure that you comment then again before deployment

Postman collection is available for better testing experience.

                                                Sincerely your,
                                                    TanyaFreeie
                                                      gitLab responsible
