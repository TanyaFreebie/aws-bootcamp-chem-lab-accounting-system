import { aws, Schema, model } from 'dynamoose';
import { v4 as uuidv4 } from 'uuid';

/* If we are on local - connect to local table */
if (process.env.STAGE === "local") {
  aws.ddb.local(process.env.DYNAMO_URL);
}
/*
  Create the schema - how the table data should look)
  This schema also validates the input data when anything is used in the Model
*/
const  ChemicalSchema = new Schema({
  "id": {
    "type": String,
    required: false,
    "hashKey": true,
    default: uuidv4
  },
  "name": { "type": String, required: true },
  "type": { "type": String, required: true },
  "subType": { "type": String, required: false },
  "count": { "type": Number, required: true },
  "comments": { "type": String, required: false },
  "amount": { "type": Number, required: false },
  "mesurement": { "type": String, required: false },
});

/* verify that all the env variables are set in .env file */
if (!process || !process.env || !process.env.CHEMICALS_TABLE_NAME) {
  throw new Error('no Chemical table name configured');
}
if (!process || !process.env || !process.env.SERVICE) {
  throw new Error('no service name configured');
}
if (!process || !process.env || !process.env.STAGE) {
  throw new Error('no stage configured');
}

const service = process.env?.SERVICE;
const stage = process.env?.STAGE;
const table = process.env?.CHEMICALS_TABLE_NAME;

/* this is how we access the data in the table, by providing table name and schema */
const Chemical = model(`${service}-${stage}-${table}`, ChemicalSchema);

export default Chemical;
