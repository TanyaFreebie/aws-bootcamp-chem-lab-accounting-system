swagger: "2.0"
info:
  description: "This is a sample server Petstore server.  You can find out more about     Swagger at [http://swagger.io](http://swagger.io) or on [irc.freenode.net, #swagger](http://swagger.io/irc/).      For this sample, you can use the api key `special-key` to test the authorization     filters."
  version: "1.0.0"
  title: "Swagger Chemestry Labaratory"
  contact:
    email: "apiteam@swagger.io"
  license:
    name: "Apache 2.0"
    url: "http://www.apache.org/licenses/LICENSE-2.0.html"
host: "petstore.swagger.io"
basePath: "/v2"
tags:
- name: "user"
  description: "Everything about users"
- name: "chemical"
  description: "Access to chemicals"
- name: "inventory"
  description: "Operations in inventory"
schemes:
- "https"
- "http"
paths:
  /chemical:
    post:
      tags:
      - "chemical"
      summary: "Create a new chemical in the lab"
      description: ""
      operationId: "addChemical"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "Chemical object that needs to be added"
        required: true
        schema:
          $ref: "#/definitions/Chemical"
      responses:
        "405":
          description: "Invalid input"
      security:
      - clas_auth:
        - "write:chemicals"
        - "read:chemicals"
    put:
      tags:
      - "chemical"
      summary: "Update an existing chemical"
      description: ""
      operationId: "putChemicalById"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "Chemical object that needs to be updated"
        required: true
        schema:
          $ref: "#/definitions/Chemical"
      responses:
        "400":
          description: "Invalid ID supplied"
        "404":
          description: "Chemical not found"
        "405":
          description: "Validation exception"
      security:
      - clas_auth:
        - "write:chemicals"
        - "read:chemicals"
  /chemical/{chemicalId}:
    get:
      tags:
      - "chemical"
      summary: "Find chemical by Id"
      description: "Returns a single chemical"
      operationId: "getChemicalById"
      produces:
      - "application/json"
      parameters:
      - name: "chemicalId"
        in: "path"
        description: "ID of chemical to return"
        required: true
        type: "string"
        format: "uuid"
      responses:
        "200":
          description: "successful operation"
          schema:
            $ref: "#/definitions/Chemical"
        "400":
          description: "Invalid ID supplied"
        "404":
          description: "Chenmical not found"
      security:
      - api_key: []
    post:
      tags:
      - "chemical"
      summary: "Updates a amount of chemical by id, amount is provided in url"
      description: ""
      operationId: "postChemicalWithForm"
      consumes:
      - "application/x-www-form-urlencoded"
      produces:
      - "application/json"
      parameters:
      - name: "chemicalId"
        in: "path"
        description: "ID of chemical that needs to be updated"
        required: true
        type: "integer"
        format: "uuid"
      - name: "amount"
        in: "formData"
        description: "Updated amount of the chemical"
        required: true
        type: "integer"
      responses:
        "405":
          description: "Invalid input"
      security:
      - clas_auth:
        - "write:chemicals"
        - "read:chemicals"
    delete:
      tags:
      - "chemical"
      summary: "Deletes a chemical"
      description: ""
      operationId: "deleteChemicalById"
      produces:
      - "application/json"
      parameters:
      - name: "api_key"
        in: "header"
        required: false
        type: "string"
      - name: "chemicalId"
        in: "path"
        description: "Chemical id to delete"
        required: true
        type: "string"
        format: "uuid"
      responses:
        "400":
          description: "Invalid ID supplied"
        "404":
          description: "Chemical not found"
      security:
      - clas_auth:
        - "write:chemicals"
        - "read:chemicals"
  /inventory:
    get:
      tags:
      - "inventory"
      summary: "Get all inventory items"
      operationId: "getInventory"
      produces:
      - "application/json"
      responses:
        "200":
          description: "successful operation"
          schema:
            $ref: "#/definitions/Inventory"
        "400":
          description: "Invalid ID supplied"
        "404":
          description: "Inventory item not found"
      security:
      - clas_auth:
        - "write:chemicals"
        - "read:chemicals"
    post:
      tags:
      - "inventory"
      summary: "Add an inventory item"
      operationId: "postInventoryItem"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "Addition of equipment of item that exist in the lab"
        required: true
        schema:
          $ref: "#/definitions/Inventory"
      responses:
        "200":
          description: "successful operation"
          schema:
            $ref: "#/definitions/Inventory"
        "400":
          description: "Invalid invetory item"
      security:
      - clas_auth:
        - "write:chemicals"
        - "read:chemicals"
    put:
      tags:
      - "inventory"
      summary: "Update a record in the inventory"
      description: "Creates or updates provided item in the body"
      operationId: "putInventoryItem"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "Partial or complete body of equipment of item that exist in the lab"
        required: true
        schema:
          $ref: "#/definitions/Inventory"
      responses:
        "200":
          description: "successful operation"
          schema:
            type: "object"
            additionalProperties:
              type: "integer"
              format: "int32"
      security:
      - clas_auth:
        - "write:chemicals"
        - "read:chemicals"
  /inventory/{inventoryId}:
    get:
      tags:
      - "inventory"
      summary: "Get one inventory items by provided id"
      operationId: "getInventoryItemById"
      produces:
      - "application/json"
      parameters:
      - name: "inventoryId"
        in: "path"
        description: "The name that needs to be fetched. Use user1 for testing. "
        required: true
        type: "string"
        format: uuid
      responses:
        "200":
          description: "successful operation"
          schema:
            $ref: "#/definitions/Inventory"
        "400":
          description: "Invalid ID supplied"
        "404":
          description: "Inventory item not found"
      security:
      - clas_auth:
        - "write:chemicals"
        - "read:chemicals"
    delete:
      tags:
      - "inventory"
      summary: "Deletes inventory item"
      operationId: "deleteInventoryItemById"
      produces:
      - "application/json"
      parameters:
      - name: "inventoryId"
        in: "path"
        description: "The id of item that needs to be deleted"
        required: true
        type: "string"
        format: uuid
      responses:
        "200":
          description: "successful operation"
      security:
      - clas_auth:
        - "write:chemicals"
        - "read:chemicals"
  /user:
    post:
      tags:
      - "user"
      summary: "Create user"
      description: "This can only be done by the logged in user."
      operationId: "postUser"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "Created user object"
        required: true
        schema:
          $ref: "#/definitions/User"
      responses:
        default:
          description: "successful operation"
  /user/login:
    get:
      tags:
      - "user"
      summary: "Logs user into the system"
      description: ""
      operationId: "getUserLogin"
      produces:
      - "application/json"
      parameters:
      - name: "username"
        in: "query"
        description: "The user name for login"
        required: true
        type: "string"
      - name: "password"
        in: "query"
        description: "The password for login in clear text"
        required: true
        type: "string"
      responses:
        "200":
          description: "successful operation"
          schema:
            type: "string"
          headers:
            X-Rate-Limit:
              type: "integer"
              format: "int32"
              description: "calls per hour allowed by the user"
            X-Expires-After:
              type: "string"
              format: "date-time"
              description: "date in UTC when token expires"
        "400":
          description: "Invalid username/password supplied"
  /user/logout:
    get:
      tags:
      - "user"
      summary: "Logs out current logged in user session"
      description: ""
      operationId: "getUserLogout"
      produces:
      - "application/json"
      parameters: []
      responses:
        default:
          description: "successful operation"
  /user/{username}:
    get:
      tags:
      - "user"
      summary: "Get user by user name"
      description: ""
      operationId: "getUserByUsername"
      produces:
      - "application/json"
      parameters:
      - name: "username"
        in: "path"
        description: "The name that needs to be fetched. Use user1 for testing. "
        required: true
        type: "string"
      responses:
        "200":
          description: "successful operation"
          schema:
            $ref: "#/definitions/User"
        "400":
          description: "Invalid username supplied"
        "404":
          description: "User not found"
    put:
      tags:
      - "user"
      summary: "Updated user"
      description: "This can only be done by the logged in user."
      operationId: "putUser"
      produces:
      - "application/json"
      parameters:
      - name: "username"
        in: "path"
        description: "name that need to be updated"
        required: true
        type: "string"
      - in: "body"
        name: "body"
        description: "Updated user object"
        required: true
        schema:
          $ref: "#/definitions/User"
      responses:
        "400":
          description: "Invalid user supplied"
        "404":
          description: "User not found"
    delete:
      tags:
      - "user"
      summary: "Delete user"
      description: "This can only be done by the logged in user."
      operationId: "deleteUser"
      produces:
      - "application/json"
      parameters:
      - name: "username"
        in: "path"
        description: "The name that needs to be deleted"
        required: true
        type: "string"
      responses:
        "400":
          description: "Invalid username supplied"
        "404":
          description: "User not found"
securityDefinitions:
  clas_auth:
    type: "oauth2"
    authorizationUrl: "http://petstore.swagger.io/oauth/dialog"
    flow: "implicit"
    scopes:
      write:chemicals: "modify pets in your account"
      read:chemicals: "read your pets"
  api_key:
    type: "apiKey"
    name: "api_key"
    in: "header"
definitions:
  Inventory:
    type: "object"
    properties:
      id:
        type: "string"
        format: "uuid"
        example: "d6ed7a80-2a15-4b75-8215-55d202fb4d69"
      description:
        type: "string"
      comments:
        type: "string"
      name:
        type: "string"
      count:
        type: "number"
      type:
        type: "string"
  User:
    type: "object"
    properties:
      id:
        type: "string"
        format: "uuid"
      username:
        type: "string"
      firstName:
        type: "string"
      lastName:
        type: "string"
      email:
        type: "string"
      password:
        type: "string"
      phone:
        type: "string"
      userStatus:
        type: "string"
        enum: 
          - active
          - deactivated
        description: "User Status"
      userRole:
        type: "string"
        enum:
          - Admin, 
          - User
        description: "User Status"
  Chemical:
    type: "object"
    required:
    - "name"
    - "type"
    - "count"
    properties:
      id:
        type: "string"
        format: "uuid"
        example: "d6ed7a80-2a15-4b75-8215-55d202fb4d69"
      name:
        type: "string"
        example: "doggie"
      type:
        type: "string"
      subType:
        type: "string"
      count:
        type: "number"
      comments:
        type: "string"
      amount:
        type: "number"
      mesurement:
        type: "string"
