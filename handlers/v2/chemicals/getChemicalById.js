import Chemical from "../../../utils/dynamo/chemical";

export default async (event) => {
    try{
        // extracting {chemicalId} from request
        const chemicalId = event.pathParameters?.chemicalId;
        // console.log to see what we got from previous step
        console.log(chemicalId);
        // getting a chemical with matching ID from the table
        const chemical = await Chemical.query("id").eq(chemicalId).exec();
        // checking if needed chemical exists in the table
        if(!chemical){
            return{
                statusCode: 404,
                body: "Chemical not found. I hope Dmitrijs did't eat it :)"
            };
        }
        return{
            statusCode: 200,
            headers: {},
            body: JSON.stringify(chemical),
            isBase64Encoded: false
        };
    }catch(error){
        // something terrible happened here
        console.error(error);
        return{
            statusCode: 500,
            headers: {},
            body: "Unexpexted error, I wish it will never happen",
            isBase64Encoded: false
        };

    }
};
