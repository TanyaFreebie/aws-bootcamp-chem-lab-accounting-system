import Chemical from "../../../utils/dynamo/chemical";
export default async (event) => {
    try{
    const body = JSON.parse(event.body);
    const chemical = await Chemical.create(body);
    return{
        statusCode: 200,
        body: JSON.stringify(chemical),
    };
    }catch(error){
        console.error(error);
        return{
            statusCode: 500,
            headers: {},
            body: "Thats unexpected",
            isBase64Encoded: false
        };
    }
};