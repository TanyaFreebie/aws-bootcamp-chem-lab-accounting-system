import Chemical from "../../../utils/dynamo/chemical";
export default async (event) => {
    try{
    // assigning body from the request to a const, for later use
    const body = JSON.parse(event.body);
    // console.log to see what we got from previous step
    console.log(body);
    // creating a new record in the table with previously recieved body
    const chemical = await Chemical.create(body);
    return{
        statusCode: 200,
        body: JSON.stringify(chemical),
    };
    }catch(error){
        // something terrible happened here
        console.error(error);
        return{
            statusCode: 500,
            headers: {},
            body: "Thats unexpected",
            isBase64Encoded: false
        };
    }
};
