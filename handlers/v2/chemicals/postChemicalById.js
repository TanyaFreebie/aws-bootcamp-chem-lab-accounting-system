import Chemical from "../../../utils/dynamo/chemical";

export default async (event) => {
    try {
        // extracting {chemicalAmount} from request
        const amount = event.queryNumberParameters?.amount;
        // extracting {chemicalId} from request
        const chemicalId = event.pathParameters?.chemicalId;
        // creating const body to hold an ID and amount
        const body = {id: chemicalId, amount};
        // checking if needed chemical exists in the table
        if(chemicalId === undefined || chemicalId.count <1) {
            return{
                statusCode: 404,
                body: "chemical id not found"
            };
        }
    // updating an amount of chemical
    const chemical = await Chemical.update(body);

    return {
        statusCode: 200,
        body: JSON.stringify(chemical),
        };
    } catch(error){
        // something terrible happened here
        console.error(error);
        return{
            statusCode: 500,
            headers: {},
            body: "Thats unexpected",
            isBase64Encoded: false
        };
    }
};