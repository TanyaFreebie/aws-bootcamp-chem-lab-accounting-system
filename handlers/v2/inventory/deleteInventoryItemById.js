import Inventory from "../../../utils/dynamo/inventory";

export default async (event) => {
  try {
  // extracting {itemId} from request
	const itemId = event.pathParameters?.itemId;
  // finding a piece of inventory in the table with the same ID as provided
	const inventory = await Inventory.query("id").eq(itemId).exec();

  // checking if needed piece of inventory exists in the table
    if (inventory === undefined) {
      /* no order found in table */
      return {
        statusCode: 404,
        headers: {},
        body: "No Inventory found",
        isBase64Encoded: false
      };
    }

    //delete the order itself if it exists;
    const deletedInventory = await Inventory.delete(itemId);
    console.log(deletedInventory);
    return {
      statusCode: 200,
      body: JSON.stringify("deleted inventory item by id"),
    };
  } catch (error) {
    // something terrible happened here
    console.error(error);
    return {
      statusCode: 500,
      headers: {},
      body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
      isBase64Encoded: false
    };
  }
};
