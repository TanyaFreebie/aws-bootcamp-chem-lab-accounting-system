import Inventory from "../../../utils/dynamo/inventory";

export default async (event) => {
  try {
    // assigning body from the request to a const, for later use
    const body = JSON.parse(event.body);
    // creating a new record in the table with previously recieved body
    const inventory = await Inventory.create(body);

    return {
      statusCode: 200,
      body: JSON.stringify(inventory),
    };
  } catch (error) {
    /* something terrible has happened */
    console.error(error);
    return {
        statusCode: 500,
        headers: {},
        body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
        isBase64Encoded: false
    };
  }
};
