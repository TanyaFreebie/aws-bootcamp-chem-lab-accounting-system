import Inventory from "../../../utils/dynamo/inventory";

export default async (event) => {
  try {
    // extracting {itemId} from request
    const itemId = event.pathParameters?.itemId;
    // getting a chemical with matching ID from the table
    const inventory = await Inventory.query("id").eq(itemId).exec();
    // checking if needed chemical exists in the table
    if (!inventory) {
        return {
            statusCode: 404,
			headers: {},
        };
    }

    return {
      statusCode: 200,
      headers: {},
      body: JSON.stringify(inventory),
      isBase64Encoded: false
    };
  } catch (error) {
    /* something terrible has happened */
    console.error(error);
    return {
      statusCode: 500,
      headers: {},
      body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
      isBase64Encoded: false
    };
  }
};
