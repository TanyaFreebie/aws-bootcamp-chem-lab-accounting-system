import Inventory from "../../../utils/dynamo/inventory";

export default async (event) => {
	try {
		// assigning body from the request to a const, for later use
		const body = JSON.parse(event.body);
		// console.log to see what we got from previous step
		console.log(body);
		// updating a record in table
		const inventory = await Inventory.update(body);
	return {
		statusCode: 200,
		body: JSON.stringify(inventory),
	};
	} catch (error) {
	/* something terrible has happened */
		console.error(error);
	return {
		statusCode: 500,
		headers: {},
		body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
		isBase64Encoded: false
	};
	}
  };