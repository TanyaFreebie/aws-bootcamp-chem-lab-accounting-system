import User from "../../../utils/dynamo/user";
import crypto from "crypto";

export default async (event) => {
  try {
    // error response if username or/and password are incorrect
    const errResp = {
        statusCode: 400,
        body: "username or password is incorrect, please try again"
    };
    // getting username and password from request
    const username = event.queryStringParameters?.username;
    const password = event.queryStringParameters?.password;
    // checking if username and password are correct
    if (!username || !password) {
        return errResp;
    }
    // checking if user is in the table
    const user = (await User.query("username").eq(username).limit(1).exec())[0];
    if (!user || user.count < 1) {
      return errResp;
    }
    // getting user from table
    const fullUser = await User.get(user.id);

    const hash = crypto.pbkdf2Sync(password, fullUser.salt, 1000, 64, `sha512`).toString(`hex`);

    if (fullUser.hash !== hash) {
        return errResp;
    }

    await User.update({id: user.id, loggedIn: true });

    return {
      statusCode: 200,
      headers: {},
      body: JSON.stringify("Success"),
      isBase64Encoded: false
    };
  } catch (error) {
    /* something terrible has happened */
    console.error(error);
    return {
        statusCode: 500,
        headers: {},
        body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
        isBase64Encoded: false
      };
    }
  };