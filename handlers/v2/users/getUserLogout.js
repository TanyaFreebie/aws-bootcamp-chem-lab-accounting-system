import User from "../../../utils/dynamo/user";

export default async (event) => {
  try {
    // assigning username from request to const username
    const username = event.queryStringParameters?.username;
    // looking for user in thele tab
    const user = (await User.query("username").eq(username).limit(1).exec())[0];
    // checking if user exists
    if (!user || user.count < 1) {
      return {
          statusCode: 404,
          body: "Can't find the user to log out"
      };
    }
    // logging user off
    await User.update({id: user.id, loggedIn: false});

    return {
        statusCode: 200,
        headers: {},
        body: "Success",
        isBase64Encoded: false
      };
    } catch (error) {
        /* something terrible has happened */
        console.error(error);
        return {
          statusCode: 500,
          headers: {},
          body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
          isBase64Encoded: false
        };
      }
    };