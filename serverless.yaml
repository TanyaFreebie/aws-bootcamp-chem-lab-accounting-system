service: aws-sls-tm-${env:TEAMNAME}
useDotenv: true
frameworkVersion: '2'
package:
  individually: true
custom:
  prefix: ${self:service}-${opt:stage}
  dynamodb:
    stages:
      - local
    start:
      port: 4000 #port on which the local database will be running
      inMemory: true
      heapInitial: 10m
      heapMax: 100m
      migrate: true
      convertEmptyValues: true
provider:
  environment:
    STAGE: ${opt:stage}
    SERVICE: ${self:service}
    TEAMNAME: ${env:TEAMNAME}
    USERS_TABLE_NAME: ${env:USERS_TABLE_NAME}
    CHEMICALS_TABLE_NAME: ${env:CHEMICALS_TABLE_NAME}
    INVENTORY_TABLE_NAME: ${env:INVENTORY_TABLE_NAME}
  name: aws
  region: ${env:AWS_REGION}
  runtime: nodejs12.x
  lambdaHashingVersion: 20201221
  iam:
    role:
      name: ${self:custom.prefix}-lambda-role
      statements:
        - Effect: Allow
          Action:
            - dynamodb:*
          Resource: '*'
functions:
  home:
    handler: www/webHandler.default
    name: ${self:custom.prefix}-webHandler
    events:
    - http:
        path: /
        method: get
    - http:
        path: /menu
        method: get
    - http:
        path: /login
        method: get
    - http:
        path: /inventory
        method: get
    - http:
        path: /inventory/add
        method: get
    - http:
        path: /chemicals
        method: get
    - http:
        path: /chemicals/add
        method: get
    - http:
        path: /css/button.css
        method: get
    - http:
        path: /css/front_page.css
        method: get
    - http:
        path: /css/header.css
        method: get
    - http:
        path: /css/main.css
        method: get
    - http:
        path: /css/normalize.css
        method: get
    - http:
        path: /css/table.css
        method: get
    - http:
        path: /img/fabicon.ico
        method: get
    - http:
        path: /img/logo.svg
        method: get
    - http:
        path: /js/main.js
        method: get
# v1 folder
#This part is commented out, 'cos it gave conflicts with API functions on deployment'
  # getInventoryHardCode:
  #   handler: handlers/v1/getInventory.default
  #   name: ${self:custom.prefix}-getInventory
  #   events:
  #   - http:
  #       path: /api/v1/inventory
  #       method: get
  # getChemicalsHardCode:
  #   handler: handlers/v1/getChemicals.default
  #   name: ${self:custom.prefix}-getChemicals
  #   events:
  #   - http:
  #       path: /api/v1/chemicals
  #       method: get
  # getUsersHardCode:
  #   handler: handlers/v1/getUsers.default
  #   name: ${self:custom.prefix}-getUsers
  #   events:
  #   - http:
  #       path: /api/v1/users
  #       method: get
# v2 folder
  postChemical:
    handler: handlers/v2/chemicals/postChemical.default
    name: ${self:custom.prefix}-postChemical
    events:
    - http:
        path: /api/v2/chemicals
        method: post
  deleteChemicalById:
    handler: handlers/v2/chemicals/deleteChemicalById.default
    name: ${self:custom.prefix}-deleteChemicalById
    events:
    - http:
        path: /api/v2/chemicals/{chemicalId}
        method: delete
  getChemicalById:
    handler: handlers/v2/chemicals/getChemicalById.default
    name: ${self:custom.prefix}-getChemicalById
    events:
    - http:
        path: /api/v2/chemicals/{chemicalId}
        method: get
  postChemicalWithForm:
    handler: handlers/v2/chemicals/postChemicalWithForm.default
    name: ${self:custom.prefix}-postChemicalWithForm
    events:
    - http:
        path: /api/v2/chemicals/createWithForm
        method: post
  putChemical:
    handler: handlers/v2/chemicals/putChemical.default
    name: ${self:custom.prefix}-putChemical
    events:
    - http:
        path: /api/v2/chemicals
        method: put
  postChemicalById:
    handler: handlers/v2/chemicals/postChemicalById.default
    name: ${self:custom.prefix}-postChemicalById
    events:
    - http:
        path: /api/v2/chemicals/{chemicalId}
        method: put

  deleteInventoryItemById:
    handler: handlers/v2/inventory/deleteInventoryItemById.default
    name: ${self:custom.prefix}-deleteInventoryItemById
    events:
    - http:
        path: /api/v2/inventory/{itemId}
        method: delete
  getInventory:
    handler: handlers/v2/inventory/getInventory.default
    name: ${self:custom.prefix}-getInventory
    events:
    - http:
        path: /api/v2/inventory
        method: get
  getInventoryItemById:
    handler: handlers/v2/inventory/getInventoryItemById.default
    name: ${self:custom.prefix}-getInventoryItemById
    events:
    - http:
        path: /api/v2/inventory/{itemId}
        method: get
  postInventoryItem:
    handler: handlers/v2/inventory/postInventoryItem.default
    name: ${self:custom.prefix}-postInventoryItem
    events:
    - http:
        path: /api/v2/inventory
        method: post
  putInventoryItem:
    handler: handlers/v2/inventory/putInventoryItem.default
    name: ${self:custom.prefix}-putInventoryItem
    events:
    - http:
        path: /api/v2/inventory
        method: put
  deleteUserByUsername:
    handler: handlers/v2/users/deleteUserByUsername.default
    name: ${self:custom.prefix}-deleteUserByUsername
    events:
    - http:
        path: /api/v2/users/{username}
        method: delete
  getUserByUsername:
    handler: handlers/v2/users/getUserByUsername.default
    name: ${self:custom.prefix}-getUserByUsername
    events:
    - http:
        path: /api/v2/users/{username}
        method: get
  getUserLogin:
    handler: handlers/v2/users/getUserLogin.default
    name: ${self:custom.prefix}-getUserLogin
    events:
    - http:
        path: /api/v2/users/login
        method: get
  getUserLogout:
    handler: handlers/v2/users/getUserLogout.default
    name: ${self:custom.prefix}-getUserLogout
    events:
    - http:
        path: /api/v2/users/logout
        method: get
  postUser:
    handler: handlers/v2/users/postUser.default
    name: ${self:custom.prefix}-postUser
    events:
    - http:
        path: /api/v2/users
        method: post
  putUser:
    handler: handlers/v2/users/putUser.default
    name: ${self:custom.prefix}-putUser
    events:
    - http:
        path: /api/v2/users
        method: put
plugins:
  - serverless-bundle
  - serverless-dynamodb-local
  - serverless-offline
resources:
  - Resources:
  - ${file(sls-config/resources/chemical.yml)}
  - ${file(sls-config/resources/inventory.yml)}
  - ${file(sls-config/resources/users.yml)}